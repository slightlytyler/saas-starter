module.exports = {
	extends: ['@saas-starter/eslint-config/base', 'airbnb', 'prettier/react'],
	env: {
		browser: true,
	},
	rules: {
		'arrow-parens': 0,
		'import/prefer-default-export': 0,
		'no-confusing-arrow': 0,
		'react/jsx-filename-extension': 0,
	},
};
