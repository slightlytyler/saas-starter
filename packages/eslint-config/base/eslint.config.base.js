module.exports = {
	parser: 'babel-eslint',
	extends: ['plugin:lodash-fp/recommended', 'prettier', 'prettier/flowtype'],
	plugins: ['flowtype-errors', 'lodash-fp', 'prettier'],
	rules: {
		'flowtype-errors/enforce-min-coverage': [2, 50],
		'flowtype-errors/show-errors': 2,
		'import/prefer-default-export': 0,
		'no-confusing-arrow': 0,
		'prettier/prettier': ['error', { singleQuote: true, trailingComma: 'all' }],
	},
	settings: {
		'flowtype-errors': {
			flowDir: '../../',
		},
		'import/resolver': {
			node: {
				moduleDirectory: ['node_modules', 'src'],
			},
		},
	},
};
