const isDevelopmentEnv = process.env.NODE_ENV === 'development';
const isTestEnv = process.env.NODE_ENV === 'test';
const modules = isTestEnv ? 'commonjs' : false;
const targets = isTestEnv
  ? { browsers: ['last 2 versions'] }
  : { node: 'current' };
const plugins = isDevelopmentEnv ? ['react-hot-loader/babel'] : [];

const reactConfig = () => ({
  presets: [
    '@saas-starter/babel-preset/base',
    [
      'env',
      {
        modules,
        targets,
      },
    ],
    'react',
  ],
  plugins,
});

module.exports = reactConfig;
