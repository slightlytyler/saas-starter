const flowRuntimePlugin = [
  'flow-runtime',
  {
    annotate: true,
    assert: true,
    warn: true,
  },
];
const stripFlowTypesPlugin = 'transform-flow-strip-types';
const plugins = process.env.NODE_ENV === 'production'
  ? [stripFlowTypesPlugin]
  : [flowRuntimePlugin, stripFlowTypesPlugin];

const baseConfig = {
  presets: ['stage-0'],
  plugins,
};

module.exports = baseConfig;
