const modules = process.env.NODE_ENV === 'test' ? 'commonjs' : false;

const nodeConfig = () => ({
  presets: [
    '@saas-starter/babel-preset/base',
    [
      'env',
      {
        modules,
        targets: {
          node: 'current',
        },
      },
    ],
  ],
});

module.exports = nodeConfig;
