const envPreset = (target => {
  switch (target) {
    case 'es':
      return [
        'env',
        {
          modules: false,
          targets: {
            node: 'current',
          },
        },
      ];

    case 'commonjs':
    default:
      return [
        'env',
        {
          modules: 'commonjs',
          targets: {
            node: 'current',
          },
        },
      ];
  }
})(process.env.BUILD_TARGET);

const libraryConfig = () => ({
  presets: ['@saas-starter/babel-preset/base', envPreset],
});

module.exports = libraryConfig;
