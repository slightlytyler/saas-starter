const graphqlConfig = () => ({
  presets: ['@saas-starter/babel-preset/node'],
});

module.exports = graphqlConfig;
