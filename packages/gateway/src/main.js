// @flow
import { graphqlExpress } from 'graphql-server-express';
import models from './models';
import schema from './schema';

const middleware = graphqlExpress({
  context: {
    models,
    user: { id: 'user1', email: 'slightlytyler@gmail.com', token: 'abc123' },
  },
  schema,
});

export default middleware;
