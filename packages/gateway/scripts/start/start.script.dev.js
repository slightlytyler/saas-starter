const path = require('path');
const bodyParser = require('body-parser');
const colors = require('colors/safe');
const cors = require('cors');
const express = require('express');
const { graphiqlExpress } = require('graphql-server-express');
const morgan = require('morgan');
const webpack = require('webpack');
const webpackNodeDevMiddleware = require('@saas-starter/webpack-node-dev-middleware');
const webpackDevConfig = require('../../config/webpack/webpack.config.dev');

const { HOST, PORT } = process.env;

const server = express();

const compiler = webpack(webpackDevConfig);

const webpackMiddleware = webpackNodeDevMiddleware(compiler, {
  report: true,
});

server.use(webpackMiddleware);

server.use(
  '/graphql',
  morgan(':method :url :status :res[content-length] - :response-time ms'),
  cors(),
  bodyParser.json(),
);

server.use(
  '/graphiql',
  graphiqlExpress({
    endpointURL: '/graphql',
  }),
);

// Catch all graphql requests
server.use('/graphql', (req, res, next) => {
  // res.locals.isomorphicCmpilation contains `stats` & `exports` properties:
  // - `stats` contains the webpack stats
  // - `exports` contains the app exports, an express middleware
  const middleware = res.locals.isomorphicCompilation.exports.default;

  try {
    middleware(req, res, next);
  } catch (err) {
    setImmediate(() => next(err));
  }
});

server.listen(80, () => {
  console.log('\n');
  console.log(colors.bold.white(`=== Gateway running at ${HOST}:${PORT} ===`));
  console.log('\n');
});
