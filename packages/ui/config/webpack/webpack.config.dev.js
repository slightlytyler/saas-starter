const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackBaseConfig = require('./webpack.config.base');

const __root = path.join(__dirname, '../../');

const webpackDevConfig = merge.multiple(webpackBaseConfig, {
  client: {
    devtool: 'inline-source-map',
    entry: [
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=false&quiet=false&noInfo=false',
      'react-hot-loader/patch',
      path.resolve(__root, 'src/main.client.js'),
    ],
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('development'),
        },
      }),
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
    ],
  },
  server: {
    devtool: 'inline-source-map',
    entry: [path.join(__root, 'src/main.server.js')],
    plugins: [
      new webpack.BannerPlugin({
        banner: 'require("source-map-support").install();',
        raw: true,
      }),
    ],
  },
});

module.exports = webpackDevConfig;
