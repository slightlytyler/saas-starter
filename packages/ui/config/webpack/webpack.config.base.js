const fs = require('fs');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');

const __root = path.join(__dirname, '../../');

const webpackRootConfig = {
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          configFile: path.join(__root, 'config/eslint/eslint.config.js'),
          emitWarning: true,
        },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader',
      },
    ],
  },
  resolve: {
    modules: ['node_modules', path.join(__root, 'src')],
  },
};

const webpackBaseConfig = {
  client: merge(webpackRootConfig, {
    name: 'client',
    target: 'web',
    output: {
      chunkFilename: '[name].js',
      filename: '[name].js',
      path: path.resolve(__root, 'build/client'),
      publicPath: '/public/',
    },
    plugins: [
      new webpack.optimize.CommonsChunkPlugin({
        names: ['bootstrap'], // needed to put webpack bootstrap code before chunks
        filename: '[name].js',
        minChunks: Infinity,
      }),
    ],
  }),
  server: merge(webpackRootConfig, {
    name: 'server',
    target: 'node',
    output: {
      path: path.join(__root, 'build/server'),
      filename: '[name].js',
      libraryTarget: 'commonjs2',
      publicPath: '/public/',
    },
    externals: fs
      .readdirSync(path.join(__root, 'node_modules'))
      .filter(
        x =>
          !/\.bin|react-universal-component|require-universal-module|webpack-flush-chunks/.test(
            x,
          ),
      )
      .reduce((externals, mod) => {
        externals[mod] = `commonjs ${mod}`;
        return externals;
      }, {}),
    plugins: [
      new webpack.optimize.LimitChunkCountPlugin({
        maxChunks: 1,
      }),
      new CopyWebpackPlugin([
        {
          from: path.join(__root, 'src/public'),
          to: '/public',
          ignore: '.DS_Store',
        },
      ]),
    ],
  }),
};

module.exports = webpackBaseConfig;
