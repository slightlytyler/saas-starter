const path = require('path');
const colors = require('colors/safe');
const express = require('express');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackIsomorphicCompiler = require('webpack-isomorphic-compiler');
const webpackIsomorphicDevMiddleware = require('webpack-isomorphic-dev-middleware');
const webpackDevConfig = require('../../config/webpack/webpack.config.dev');
const serveStaticDevMiddleware = require('../../src/core/ssr/serveStaticDevMiddleware');

const { HOST, PORT } = process.env;

const server = express();

const isomorphicCompiler = webpackIsomorphicCompiler(...webpackDevConfig);

const webpackMiddleware = webpackIsomorphicDevMiddleware(isomorphicCompiler, {
  report: true,
});

const hotMiddleware = webpackHotMiddleware(
  isomorphicCompiler.client.webpackCompiler,
);

const serveStaticMiddleware = serveStaticDevMiddleware(
  '/public',
  isomorphicCompiler.server.webpackCompiler.outputFileSystem,
);

server.use('/', serveStaticMiddleware);

server.use(webpackMiddleware);

server.use(hotMiddleware);

// Catch all route to attempt to render our isomorphic app
server.get('*', (req, res, next) => {
  // res.locals.isomorphicCompilation contains `stats` & `exports` properties:
  // - `stats` contains the client & server stats
  // - `exports` contains the server exports, usually one or more render functions
  const renderServer = res.locals.isomorphicCompilation.exports.default;

  renderServer({
    clientStats: res.locals.isomorphicCompilation.stats.client.toJson(),
  })(req, res).catch(err => setImmediate(() => next(err)));
});

server.listen(80, () => {
  console.log('\n');
  console.log(colors.bold.white(`=== UI running at ${HOST}:${PORT} ===`));
  console.log('\n');
});
