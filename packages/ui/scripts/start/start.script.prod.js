// NOTE: This shit don't work. Look at the dev script and adapt.
const path = require('path');
const colors = require('colors/safe');
const express = require('express');
const webpackProdConfig = require('../../config/webpack/webpack.config.prod');

const { HOST, PORT } = process.env;
const { path: outputPath, publicPath } = clientConfig.output;

const server = express();

const clientStats = require(path.join(outputPath, 'client/stats.json'));

const renderServer = require(path.join(outputPath, 'server/main.js'));

server.use(
  '/',
  express.static(path.join(__dirname, '../../src/public'), {
    maxAge: 0,
    etag: false,
  }),
);

server.use(renderServer({ clientStats }));

server.listen(80, () => {
  console.log('\n');
  console.log(colors.bold.white(`=== UI running at ${HOST}:${PORT} ===`));
  console.log('\n');
});
