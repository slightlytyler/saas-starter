// @flow
import type { $Request, $Response } from 'express';
import { get } from 'lodash/fp';
import React from 'react';
import {
  ApolloClient,
  ApolloProvider,
  createNetworkInterface,
  renderToStringWithData,
} from 'react-apollo';
import { StaticRouter } from 'react-router-dom';
import { flushChunkNames } from 'react-universal-component/server';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import flushChunks from 'webpack-flush-chunks';
import stringifyWindowState from 'core/ssr/stringifyWindowState';
import authNetworkInterfaceMiddleware from 'features/auth/network-interface-middleware';
import Root from './Root';

const renderServer = ({ clientStats }: { clientStats: any }) => (
  req: $Request,
  res: $Response,
) => {
  const networkInterface = createNetworkInterface({
    uri: process.env.API_LOCAL_URL,
  });
  networkInterface.use([authNetworkInterfaceMiddleware]);
  const client = new ApolloClient({
    dataIdFromObject: get('id'),
    networkInterface,
    ssrMode: true,
  });
  const store = createStore(
    combineReducers({
      apollo: client.reducer(),
    }),
    {},
    applyMiddleware(client.middleware()),
  );
  const routerContext = {};

  return renderToStringWithData(
    <ApolloProvider client={client} store={store}>
      <StaticRouter location={req.url} context={routerContext}>
        <Root />
      </StaticRouter>
    </ApolloProvider>,
  ).then(rootElement => {
    if (routerContext.url) {
      res.writeHead(301, {
        Location: routerContext.url,
      });
      res.end();
    } else {
      const env = {
        API_URL: process.env.API_REMOTE_URL,
        INITIAL_STATE: store.getState(),
      };
      const { js } = flushChunks(clientStats, {
        chunkNames: flushChunkNames(),
        before: ['bootstrap'],
        after: ['main'],
      });

      res.write(
        `
        <!DOCTYPE html>
        <html>
          <head>
            <meta charset="utf-8">
            <title>🚀 DTR-esque 🚀</title>
            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css"></link>
            <style>
              .react-layout-components--box {
                display: -webkit-box;
                display: -moz-box;
                display: -ms-flexbox;
                display: -webkit-flex;
                display: flex;
              }
            </style>
          </head>
          <body>
            <div id="root">${rootElement}</div>
            <script>window.env = ${stringifyWindowState(env)};</script>
            ${js.toString()}
          </body>
        </html>
      `,
      );
      res.end();
    }
  });
};

export default renderServer;
