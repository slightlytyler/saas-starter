import React from 'react';
import { Box } from 'react-layout-components';
import { branch, renderComponent } from 'recompose';
import { Dimmer, Loader } from 'semantic-ui-react';

const DefaultLoader = () =>
  <Box fit style={{ position: 'relative' }}>
    <Dimmer active inverted>
      <Loader inverted size="massive">
        Loading...
      </Loader>
    </Dimmer>
  </Box>;

const withLoadingState = (
  conditionSelector,
  LoadingComponent = DefaultLoader,
) => branch(conditionSelector, renderComponent(LoadingComponent));

export default withLoadingState;
