// @flow
import type { Element } from 'react';

const renderNoop = () => null;

type BranchProps = {
  condition: boolean,
  renderLeft: () => Element<any>,
  renderRight?: () => ?Element<any>,
};

const Branch = ({ renderRight = renderNoop, ...props }: BranchProps) =>
  props.condition ? props.renderLeft() : renderRight();

export default Branch;
