// @flow
import React from 'react';
import { Image } from 'semantic-ui-react';

type AvatarProps = {
  name: string,
  src: string,
};

const Avatar = (props: AvatarProps) =>
  <div>
    <Image avatar src={props.src} />
    <span>{props.name}</span>
  </div>;

export default Avatar;
