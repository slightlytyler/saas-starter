// @flow
import PropTypes from 'prop-types';
import React from 'react';
import { Label } from 'semantic-ui-react';
import Branch from 'common/components/Branch';

type HelpBlockProps = {
  error: boolean,
  info: boolean,
  message: ?string,
  warning: boolean,
};

const selectColor = (props: HelpBlockProps) => {
  if (props.error) return 'red';
  if (props.warning) return 'yellow';
  if (props.info) return 'blue';
  return 'blue';
};

const HelpBlock = (props: HelpBlockProps) =>
  <Branch
    condition={Boolean(props.message)}
    renderLeft={() =>
      <Label basic color={selectColor(props)} pointing>{props.message}</Label>}
  />;

export default HelpBlock;
