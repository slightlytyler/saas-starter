import PropTypes from 'prop-types';
import elementArrayPropType from 'common/prop-types/elementArrayPropType';

const childrenPropType = PropTypes.oneOfType([
  PropTypes.element,
  elementArrayPropType,
]);

export default childrenPropType;
