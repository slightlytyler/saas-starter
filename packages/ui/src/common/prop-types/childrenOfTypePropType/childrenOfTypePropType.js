import {
  compose,
  every,
  get,
  indexOf,
  isArray,
  isPlainObject,
  join,
  map,
} from 'lodash/fp';
import createPropType from 'common/utilities/createPropType';

const contains = strings => value => indexOf(strings, value) !== -1;

const everyContains = (strings, values) => every(contains(strings), values);

const printNames = compose(join(', '), map(n => `\`${n}\``));

const childrenOfTypePropType = Components => {
  const names = map(get('name'), Components);
  const validator = (props, propName, componentName) => {
    const childrenProp = props[propName];
    if (
      (isPlainObject(childrenProp) && contains(names)(childrenProp.name)) ||
      (isArray(childrenProp) &&
        everyContains(names, map(get('type.name'), childrenProp)))
    ) {
      return Error(
        `Invalid prop \`${propName}\` supplied to \`${componentName}\`. \`${propName}\` elements must be one of types ${printNames(
          names,
        )}.`,
      );
    }
    return undefined;
  };
  return createPropType(validator);
};

export default childrenOfTypePropType;
