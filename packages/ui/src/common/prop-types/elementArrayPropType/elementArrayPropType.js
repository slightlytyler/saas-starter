import PropTypes from 'prop-types';

const elementArrayPropType = PropTypes.arrayOf(PropTypes.element);

export default elementArrayPropType;
