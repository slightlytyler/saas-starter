import { has } from 'lodash/fp';

const createPropType = validator => {
  // eslint-disable-next-line no-param-reassign
  validator.isRequired = (props, propName, componentName) => {
    if (!has(propName, props)) {
      return Error(
        `The prop \`${propName}\` is marked as required in \`${componentName}\`, but its value is \`${props[
          propName
        ]}\`.`,
      );
    }
    return undefined;
  };
  return validator;
};

export default createPropType;
