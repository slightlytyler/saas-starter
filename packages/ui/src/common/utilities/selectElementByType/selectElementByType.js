import { find, isPlainObject } from 'lodash/fp';

const selectElementByType = (Component, elements) => {
  if (!Component) {
    throw Error(
      `\`selectElementByType\` requires a valid React Component as it's first argument, you passed \`${Component}\``,
    );
  }
  if (!elements) return null;
  if (isPlainObject(elements)) {
    if (Component.name !== elements.type.name) return null;
    return elements;
  }
  const element = find(e => Component.name === e.type.name, elements);
  if (!element) return null;
  return element;
};

export default selectElementByType;
