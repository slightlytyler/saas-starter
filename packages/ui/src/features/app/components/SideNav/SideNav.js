// @flow
import React from 'react';
import { NavLink } from 'react-router-dom';
import { Menu } from 'semantic-ui-react';

const SideNav = () =>
  <Menu attached style={{ borderTopWidth: 0, height: '100%' }} vertical>
    <Menu.Item as={NavLink} to="/repositories">Repositories</Menu.Item>
    <Menu.Item as={NavLink} to="/organizations">Organizations</Menu.Item>
    <Menu.Item as={NavLink} to="/users">Users</Menu.Item>
    <Menu.Item as={NavLink} to="/settings">Settings</Menu.Item>
  </Menu>;

export default SideNav;
