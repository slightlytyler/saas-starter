// @flow
import React from 'react';
import { Box } from 'react-layout-components';
import { Link } from 'react-router-dom';
import { Search, Segment } from 'semantic-ui-react';
import Avatar from 'common/components/Avatar';
import Logo from 'features/app/components/Logo';
import AuthenticatedBranch from 'features/auth/components/AuthenticatedBranch';
import LoginButton from 'features/auth/components/LoginButton';

const SLIGHTLYTYLER_PROFILE =
  'https://avatars0.githubusercontent.com/u/6819171?v=3&s=40';

const TopNav = () =>
  <Segment attached>
    <Box alignItems="center" justifyContent="space-between">
      <Box alignItems="center">
        <Link to="/">
          <Logo />
        </Link>
      </Box>
      <Box center flex="1">
        <Search placeholder="Looking for something?" />
      </Box>
      <AuthenticatedBranch
        renderLeft={() =>
          <Box>
            <Avatar name="slightlytyler" src={SLIGHTLYTYLER_PROFILE} />
          </Box>}
        renderRight={() => <LoginButton />}
      />
    </Box>
  </Segment>;

export default TopNav;
