// @flow
import React from 'react';
import Emojify from 'react-emojione';

const Logo = () =>
  <Emojify style={{ height: 44, width: 44 }}>:stopwatch:</Emojify>;

export default Logo;
