// @flow
import React from 'react';
import type { ReactChildren } from 'react-flow-types';
import { Box } from 'react-layout-components';

type AppLayoutContentAreaProps = {
  children: ReactChildren,
};

const AppLayoutContentArea = (props: AppLayoutContentAreaProps) =>
  <Box column fit style={{ padding: '1em' }}>
    {props.children}
  </Box>;

export default AppLayoutContentArea;
