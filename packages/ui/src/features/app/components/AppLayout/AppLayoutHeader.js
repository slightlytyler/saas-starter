// @flow
import React from 'react';
import type { ReactChildren } from 'react-flow-types';

type AppLayoutHeaderProps = {
  children: ReactChildren,
};

const AppLayoutHeader = (props: AppLayoutHeaderProps) =>
  <div style={{ width: '100%' }}>{props.children}</div>;

export default AppLayoutHeader;
