// @flow
import React from 'react';
import type { ReactChildren } from 'react-flow-types';

type AppLayoutSidePanelProps = {
  children: ReactChildren,
};

const AppLayoutSidePanel = (props: AppLayoutSidePanelProps) =>
  <div>{props.children}</div>;

export default AppLayoutSidePanel;
