// @flow
import React from 'react';
import type { Element } from 'react-flow-types';
import { Box, Page } from 'react-layout-components';
import selectElementByType from 'common/utilities/selectElementByType';
import AppLayoutContentArea from './AppLayoutContentArea';
import AppLayoutHeader from './AppLayoutHeader';
import AppLayoutSidePanel from './AppLayoutSidePanel';

type AppLayoutProps = {
  children: Array<
    Element<AppLayoutContentArea | AppLayoutHeader | AppLayoutSidePanel>,
  >,
};

const AppLayout = (props: AppLayoutProps) =>
  <Page>
    <Box column fit>
      <Box>
        {selectElementByType(AppLayoutHeader, props.children)}
      </Box>
      <Box flex="1">
        {selectElementByType(AppLayoutSidePanel, props.children)}
        {selectElementByType(AppLayoutContentArea, props.children)}
      </Box>
    </Box>
  </Page>;

AppLayout.ContentArea = AppLayoutContentArea;
AppLayout.Header = AppLayoutHeader;
AppLayout.SidePanel = AppLayoutSidePanel;

export default AppLayout;
