// @flow
import React from 'react';
import { Button } from 'semantic-ui-react';

const githubClientId = 'ee05e559ad7bc05d8998';

const navigateToGithub = () => {
  location.href = `https://github.com/login/oauth/authorize?client_id=${githubClientId}`;
};

const LoginButton = () => <Button onClick={navigateToGithub}>Login</Button>;

export default LoginButton;
