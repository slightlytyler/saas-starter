// @flow
import type { Element, HigherOrderComponent } from 'react-flow-types';
import { withProps } from 'recompose';
import Branch from 'common/components/Branch';

type AuthenticatedBranchContainerRequiredProps = {
  renderLeft: () => Element<any>,
  renderRight?: () => Element<any>,
};

type AuthenticatedBranchContainerProvidedProps = {
  condition: boolean,
};

const container: HigherOrderComponent<
  AuthenticatedBranchContainerRequiredProps,
  AuthenticatedBranchContainerProvidedProps,
> = withProps({
  condition: false,
});

export default container(Branch);
