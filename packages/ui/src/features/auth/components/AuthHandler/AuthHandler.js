// @flow
import React from 'react';
import { Box } from 'react-layout-components';
import { Dimmer, Loader } from 'semantic-ui-react';

const AuthHandler = () =>
  <Box center fit>
    <Dimmer active inverted>
      <Loader inverted size="massive">
        Hold on to your stopwatch...
      </Loader>
    </Dimmer>
  </Box>;

export default AuthHandler;
