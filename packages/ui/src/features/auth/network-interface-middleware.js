// @flow
const authNetworkInterfaceMiddleware = {
  applyMiddleware(req: { ctx?: { token?: string } }, next: () => void) {
    if (!req.ctx) req.ctx = {};
    req.ctx.token = 'a1b2c3d4e5';
    next();
  },
};

export default authNetworkInterfaceMiddleware;
