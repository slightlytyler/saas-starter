// @flow
import React from 'react';
import AuthHandler from 'features/auth/components/AuthHandler';

const AuthRedirectScene = () => <AuthHandler />;

export default AuthRedirectScene;
