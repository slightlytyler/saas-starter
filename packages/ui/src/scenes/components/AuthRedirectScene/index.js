// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncAuthRedirectScene: Component<any> = universal(
  () =>
    import(/* webpackChunkName: 'AuthRedirectScene' */ './AuthRedirectScene'),
  {
    chunkName: 'AuthRedirectScene',
    resolve: () => require.resolveWeak('./AuthRedirectScene'),
  },
);

export default AsyncAuthRedirectScene;
