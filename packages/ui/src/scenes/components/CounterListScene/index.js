// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncCounterListScene: Component<any> = universal(
  () => import(/* webpackChunkName: 'CounterListScene' */ './CounterListScene'),
  {
    chunkName: 'CounterListScene',
    resolve: () => require.resolveWeak('./CounterListScene'),
  },
);

export default AsyncCounterListScene;
