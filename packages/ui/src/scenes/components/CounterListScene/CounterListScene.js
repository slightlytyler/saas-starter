// @flow
import React from 'react';
import CounterList from 'features/counter/components/CounterList';

const CounterListScene = () => <CounterList />;

export default CounterListScene;
