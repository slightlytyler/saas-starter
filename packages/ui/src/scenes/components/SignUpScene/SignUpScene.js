// @flow
import React from 'react';
import SignUpForm from 'features/auth/components/SignUpForm';

const SignUpScene = () => <SignUpForm />;

export default SignUpScene;
