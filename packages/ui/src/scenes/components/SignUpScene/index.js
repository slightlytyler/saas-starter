// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncSignUpScene: Component<any> = universal(
  () => import(/* webpackChunkName: 'SignUpScene' */ './SignUpScene'),
  {
    chunkName: 'SignUpScene',
    resolve: () => require.resolveWeak('./SignUpScene'),
  },
);

export default AsyncSignUpScene;
