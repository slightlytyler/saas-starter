// @flow
import React from 'react';
import { Box } from 'react-layout-components';
import { Header, Image } from 'semantic-ui-react';

const HomePage = () =>
  <Box center column fit>
    <Header as="h1">
      Counter as a Service (Caa$)
    </Header>
    <Header as="h3">
      <span aria-label="rocketship" role="img">🚀</span>
      &nbsp;
      Built with React + Apollo + Semantic UI
      &nbsp;
      <span aria-label="rocketship" role="img">🚀</span>
    </Header>
    <Box
      alignItems="center"
      justifyContent="space-between"
      style={{ width: '420px' }}
    >
      <Image
        src="https://raw.githubusercontent.com/reactjs/redux/master/logo/logo.png"
        style={{ width: '60px' }}
      />
      <Image
        src="https://cdn.worldvectorlogo.com/logos/react-router.svg"
        style={{ width: '60px' }}
      />
      <Image
        src="https://seeklogo.com/images/R/react-logo-7B3CE81517-seeklogo.com.png"
        style={{ width: '60px' }}
      />
      <Image
        src="https://seeklogo.com/images/A/apollo-logo-DC7DD3C444-seeklogo.com.png"
        style={{ width: '60px' }}
      />
      <Image
        src="https://upload.wikimedia.org/wikipedia/commons/1/17/GraphQL_Logo.svg"
        style={{ width: '60px' }}
      />
    </Box>
  </Box>;

export default HomePage;
