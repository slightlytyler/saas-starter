// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncHomePageScene: Component<any> = universal(
  () => import(/* webpackChunkName: 'HomePageScene' */ './HomePageScene'),
  {
    chunkName: 'HomePageScene',
    resolve: () => require.resolveWeak('./HomePageScene'),
  },
);

export default AsyncHomePageScene;
