// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncNotFoundScene: Component<any> = universal(
  () => import(/* webpackChunkName: 'NotFoundScene' */ './NotFoundScene'),
  {
    chunkName: 'NotFoundScene',
    resolve: () => require.resolveWeak('./NotFoundScene'),
  },
);

export default AsyncNotFoundScene;
