// @flow
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import type { Match } from 'react-router-dom';
import CounterListScene from 'scenes/components/CounterListScene';

type CounterRootSceneProps = {
  match: Match,
};

const CounterRootScene = (props: CounterRootSceneProps) =>
  <Switch>
    <Route component={CounterListScene} exact path={props.match.url} />
  </Switch>;

export default CounterRootScene;
