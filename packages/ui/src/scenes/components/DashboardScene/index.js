// @flow
import type { Component } from 'react-flow-types';
import universal from 'react-universal-component';

const AsyncDashboardScene: Component<any> = universal(
  () => import(/* webpackChunkName: 'DashboardScene' */ './DashboardScene'),
  {
    chunkName: 'DashboardScene',
    resolve: () => require.resolveWeak('./DashboardScene'),
  },
);

export default AsyncDashboardScene;
