// @flow
import { compose, get, identity, isUndefined } from 'lodash/fp';
import React from 'react';
import {
  ApolloClient,
  ApolloProvider,
  createNetworkInterface,
} from 'react-apollo';
import ReactDOM from 'react-dom';
import AppContainer from 'react-hot-loader/lib/AppContainer';
import { BrowserRouter } from 'react-router-dom';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import authNetworkInterfaceMiddleware from 'features/auth/network-interface-middleware';
import Root from './Root';

const networkInterface: {
  use: (middlewares: Array<*>) => void,
} = createNetworkInterface({
  uri: window.env.API_URL,
});

networkInterface.use([authNetworkInterfaceMiddleware]);

const client = new ApolloClient({
  dataIdFromObject: get('id'),
  networkInterface,
});

/* eslint-disable no-underscore-dangle */
const store = createStore(
  combineReducers({
    apollo: client.reducer(),
  }),
  window.env.INITIAL_STATE,
  compose(
    applyMiddleware(client.middleware()),
    !isUndefined(window.__REDUX_DEVTOOLS_EXTENSION__)
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : identity,
  ),
);
/* eslint-enable no-underscore-dangle */

const renderClient = () => {
  ReactDOM.render(
    <AppContainer>
      <ApolloProvider client={client} store={store}>
        <BrowserRouter>
          <Root />
        </BrowserRouter>
      </ApolloProvider>
    </AppContainer>,
    document.querySelector('#root'),
  );
};

renderClient();

if (module.hot) {
  module.hot.accept('./Root', () => {
    renderClient();
  });
}
