// @flow
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import AppLayout from 'features/app/components/AppLayout';
import TopNav from 'features/app/components/TopNav';
import SideNav from 'features/app/components/SideNav';
import AuthenticatedBranch from 'features/auth/components/AuthenticatedBranch';
import AuthRedirectScene from 'scenes/components/AuthRedirectScene';
import CounterListScene from 'scenes/components/CounterListScene';
import DashboardScene from 'scenes/components/DashboardScene';
import HomePageScene from 'scenes/components/HomePageScene';
import NotFoundScene from 'scenes/components/NotFoundScene';
import SignUpScene from 'scenes/components/SignUpScene';

const Root = () =>
  <AppLayout>
    <AppLayout.Header>
      <TopNav />
    </AppLayout.Header>
    <AuthenticatedBranch
      renderLeft={() =>
        <AppLayout.SidePanel>
          <SideNav />
        </AppLayout.SidePanel>}
    />
    <AppLayout.ContentArea>
      <AuthenticatedBranch
        renderLeft={() =>
          <Switch>
            <Route component={DashboardScene} exact path="/" />
            <Route component={CounterListScene} path="/counters" />
            <Route component={NotFoundScene} />
          </Switch>}
        renderRight={() =>
          <Switch>
            <Route component={HomePageScene} exact path="/" />
            <Route component={AuthRedirectScene} path="/auth" />
            <Route component={SignUpScene} path="/sign-up" />
            <Route component={NotFoundScene} />
          </Switch>}
      />
    </AppLayout.ContentArea>
  </AppLayout>;

export default Root;
