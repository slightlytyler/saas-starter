import React from 'react';
import { Box } from 'react-layout-components';
import { Dropdown } from 'semantic-ui-react';

const options = [
  { key: 5, value: 5, text: 5 },
  { key: 10, value: 10, text: 10 },
  { key: 20, value: 20, text: 20 },
  { key: 50, value: 50, text: 50 },
];

const PaginationLimiter = () =>
  <Box alignItems="center">
    <span>
      Show
      &nbsp;
    </span>
    <Dropdown
      fluid
      options={options}
      selection
      style={{ flex: 1 }}
      value={20}
    />
    <span>
      &nbsp;
      entries
    </span>
  </Box>;

export default PaginationLimiter;
