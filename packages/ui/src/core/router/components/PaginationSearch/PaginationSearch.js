import React from 'react';
import { Input } from 'semantic-ui-react';

const PaginationSearch = () => <Input icon="search" placeholder="Search..." />;

export default PaginationSearch;
