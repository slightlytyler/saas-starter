import React from 'react';
import { Box } from 'react-layout-components';

const PaginationCount = () => <Box>Showing 1 to 10 of 100 entries</Box>;

export default PaginationCount;
