import { noop } from 'lodash/fp';
import React from 'react';
import { Icon, Menu } from 'semantic-ui-react';

const PaginationButtons = () =>
  <Menu style={{ margin: 0 }}>
    <Menu.Item onClick={noop}>
      <Icon name="caret left" />
    </Menu.Item>
    <Menu.Item disabled>...</Menu.Item>
    <Menu.Item onClick={noop}>
      <Icon name="caret right" />
    </Menu.Item>
  </Menu>;

export default PaginationButtons;
