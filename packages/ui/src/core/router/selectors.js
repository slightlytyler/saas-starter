import { get, pick } from 'lodash/fp';
import qs from 'qs';
import { createSelector } from 'reselect';

// (Location) => searchString
export const selectSearch = get('search');

// (Locations) => params
export const selectParams = createSelector(selectSearch, search =>
  qs.parse(search, {
    ignoreQueryPrefix: true,
  }),
);

// (Location) => paginationParams
export const selectPaginationParams = createSelector(
  selectParams,
  pick(['after', 'before', 'first', 'last']),
);
