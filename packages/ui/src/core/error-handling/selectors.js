import { compose, get, map, toPairs } from 'lodash/fp';
import React from 'react';
import { List } from 'semantic-ui-react';

export const selectError = get('graphQLErrors[0].originalError');

export const selectErrorReasonList = compose(
  map(([key, val]) => <List.Item key={key}>{key} - {val}</List.Item>),
  toPairs,
);
