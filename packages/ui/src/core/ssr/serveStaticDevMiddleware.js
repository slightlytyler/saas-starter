const localFS = require('fs');
const path = require('path');
const mime = require('mime-types');

const serveStaticDevMiddleware = (directory, fs) => (req, res, next) => {
  const file = path.join(directory, req.path);
  fs.readFile(file, null, (err, fd) => {
    if (err || !fd) next();
    else {
      const ext = path.extname(file);
      const type = mime.lookup(ext);
      res.set('Content-Type', type);
      res.end(fd);
    }
  });
};

module.exports = serveStaticDevMiddleware;
