import { compose, replace } from 'lodash/fp';

const stringifyWindowState = compose(replace(/</g, '\\u003c'), JSON.stringify);

export default stringifyWindowState;
