import { map, get } from 'lodash/fp';

const selectNodesFromEdges = map(get('node'));

export default selectNodesFromEdges;
