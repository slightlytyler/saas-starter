const { merge } = require('lodash/fp');
const mainMiddleware = require('webpack-isomorphic-dev-middleware/lib/mainMiddleware');
const memoryFs = require('webpack-isomorphic-dev-middleware/lib/fs/memoryFs');
const standardFs = require('webpack-isomorphic-dev-middleware/lib/fs/standardFs');
const webpackIsomorphicServerCompiler = require('./compiler');

const parseArgs = (compiler, options) => ({
	compiler: webpackIsomorphicServerCompiler(compiler, compiler),
	options: parseOptions(options),
});

const parseOptions = options => {
	options = merge(
		{
			memoryFs: true, // Enable memory fs
			report: { stats: 'once' }, // Enable reporting, see https://github.com/moxystudio/webpack-isomorphic-compiler/blob/master/README.md#reporter
			watchOptions: {}, // Options to pass to .watch()
			headers: null, // Headers to set when serving compiled files, see https://github.com/webpack/webpack-dev-middleware
		},
		options,
	);

	Object.assign(options.watchOptions, { report: options.report });

	return options;
};

const middleware = (...args) => {
	const { compiler, options } = parseArgs(...args);

	// Set output filesystems
	const fs = options.memoryFs ? memoryFs() : standardFs();

	compiler.client.webpackCompiler.outputFileSystem = fs;
	compiler.server.webpackCompiler.outputFileSystem = fs;

	// Create middleware by composing our parts
	const middleware = mainMiddleware(compiler, options);

	// Start watching
	options.watchOptions !== false && compiler.watch(options.watchOptions);

	// Expose compiler
	middleware.compiler = compiler;

	return middleware;
};

module.exports = middleware;
